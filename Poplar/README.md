# Poplar

Poplar is a Python applicaion for data analysis.

## Installation

Use the package manager [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/) to install python environment.

```bash
conda create --prefix <path_to_env> --file spec-file.txt
```
spec-file.txt is in the root source directory

or

```bash
conda create --prefix <path_to_env> python=3.8 pandas numpy scipy matplotlib
conda activate <path_to_env>
conda install -c conda-forge pyside2
conda install -c anaconda pandas-datareader
```

Download from the app from https://bitbucket.org/alkatzo/apps/downloads/ 


# Usage
```python
<path_to_env>/bin/python Poplar/Main.pyc
```


## License
[GPL](https://choosealicense.com/licenses/mit/)
